import org.junit.Test;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import static org.junit.Assert.*;

public class MyListTest {
    
    String[] tab = new String[] {null};
    MyList<String> emptyTab = new MyList<>();

    String[] t = new String[] {null};
    MyList<String> tabWithNull = new MyList<>(t);
    
    String[] strings = new String[] {"ma", "ta", "bla", "kiki", "ma", "koko"};
    MyList<String> myListStrings = new MyList<>(strings);
    
    Integer[] ints = new Integer[] {1,2,3,4,5};
    MyList<Integer> myListInteger = new MyList<>(ints);

    Person[] persons = new Person[] {
            new Person("Ala", 24, true),
            new Person("Ala", 24, true),
            new Person("Franek", 13, false),
            new Person("Giena", 67, false)
    };
    MyList<Person> myListPersons = new MyList<>(persons);
    
    @Test
    public void theSameSize() {
        String[] tab = new String[] {"ma", "ta", "bla", "kiki","ma", "koko"};        
        assertEquals(tab.length, myListStrings.size());

        Person[] persons = new Person[] {
                new Person("Ala", 24, true),
                new Person("Ala", 24, true),
                new Person("Franek", 13, false),
                new Person("Giena", 67, false)
        };
        assertEquals(persons.length, myListPersons.size());
        
        Integer[] ints = new Integer[] {1,2,3,4,5};
        assertEquals(ints.length, myListInteger.size());
    }
    
    @Test
    public void isEmpty() {
        assertEquals(false, myListStrings.isEmpty());
        assertEquals(false, myListPersons.isEmpty());
        assertEquals(true, emptyTab.isEmpty());
    }
    
    @Test
    public void doesContainTheParticularElement(){
        assertTrue(myListStrings.contains("ma"));
        Person p = new Person("Ala", 24, true);
        assertTrue(myListPersons.contains(p));        
        assertTrue(tabWithNull.contains(null));       
        assertFalse(tabWithNull.contains("mama"));

        String[] tab2 = new String[] {null, "fikimiki"};
        MyList<String> tabWithNullAndOneValue = new MyList<>(tab2);
        assertTrue(tabWithNullAndOneValue.contains("fikimiki"));       
    }
    
    @Test
    public void doesCreateCorrectlyArray() {
        String[] tab = new String[] {"ma", "ta", "bla", "kiki","ma", "koko"};
        String[] checkingTab = (String[]) myListStrings.toArray();
        for (int i = 0; i < tab.length; i++) {
            assertEquals(tab[i], checkingTab[i]);
        }
    }

    
    @Test 
    public void doesCreateCorrectlyArrayFromArray() throws Exception{         
        MyList<String> list = new MyList<>(new String[] {"mama", "tata", "bubu"});
        String[] tab1 = new String[] {"bu", "fu", "fru"};
        String[] tab2 = new String[] {"koza"};
        String[] tab3 = new String[] {"buty", "drzwi", "szafa", "buda", "kura"};
        String[] tab4 = new String[] {null};

        Object[] strings = list.toArray(tab1);
        String[] checkingTheSameLenght = new String[] {"mama", "tata", "bubu"};
        for (int i = 0; i < strings.length; i++) {
            assertEquals(strings[i], checkingTheSameLenght[i]);
        }

        Object[] strings2 = list.toArray(tab2);
        String[] checkingShorter = new String[] {"mama", "tata", "bubu"};
        for (int i = 0; i < strings2.length; i++) {
            assertEquals(strings2[i], checkingShorter[i]);
        }

        Object[] strings3 = list.toArray(tab3);
        String[] checkingLonger = new String[] {"mama", "tata", "bubu", null, null};
        for (int i = 0; i < strings3.length; i++) {
            assertEquals(strings3[i], checkingLonger[i]);
        }
    }
    
    
    @Test
    public void doesAddCorrectly() {        
        assertTrue(myListStrings.add(null));
        Object[] objects = (Object[]) myListStrings.getTab();
        String[] strings = new String[] {"ma", "ta", "bla", "kiki", "ma", "koko", null};
        for (int i = 0; i < strings.length; i++) {
            assertEquals(strings[i], objects[i]);
        }
        
        assertTrue(myListPersons.add(new Person("Blanka", 50, true)));
        
        String[] tab = new String[] {};
        assertEquals(tab.length, emptyTab.size());        
    }
    
    @Test
    public void doesRemoveCorrectly() {
        String[] strings = new String[] {"ma", "ta", "bla", "kiki", "ma", "koko"};
        MyList<String> list = new MyList<>(strings);
        assertTrue(list.remove("ma"));       
        Object[] objects = (Object[]) list.getTab();
        String[] tmp = new String[] {"ta", "bla", "kiki","ma", "koko"};
        for (int i = 0; i < tmp.length; i++) {
            assertEquals(tmp[i], objects[i]);
        }
        Person p = new Person("Ala", 24, true);
        assertTrue(myListPersons.remove(p));
        String[] tab = new String[] {"mama", "ma", "kota", null};
        MyList<String> nullToRemove = new MyList<>(tab);
        assertTrue(nullToRemove.remove(null));
    }
    
    @Test
    public void doesContainAllElements() {
        List<String> list = new ArrayList<>();
        list.add("ma");
        list.add("ta");
        list.add("bla");
        list.add("kiki");
        list.add("ma");
        list.add("koko"); 
        assertTrue(myListStrings.containsAll(list));

        Integer[] ints = new Integer[] {1,1,2,3};
        MyList<Integer> myListInteger = new MyList<>(ints);
        List<Integer> collection = new ArrayList<>();
        collection.add(1);
        collection.add(3);
        assertEquals(true, myListInteger.containsAll(collection));

        
        List<Person> listOfPerson = new ArrayList<>();
        listOfPerson.add(new Person("Ala", 24, true));
        listOfPerson.add(new Person("Ala", 24, true));
        listOfPerson.add(new Person("Franek", 13, false));
        listOfPerson.add(new Person("Giena", 67, false));
        assertTrue(myListPersons.containsAll(listOfPerson));

        List<String> list2 = new ArrayList<>();
        list2.add("tata");
        list2.add("kupił");
        list2.add("auto");
        list2.add(null);
        list2.add("szara");
        String[] strings = new String[] {"tata", "kupił", "auto", null};
        MyList<String> myList = new MyList<>(strings);
        assertTrue(list2.containsAll(myList));
    }
    
    @Test
    public void doesAddCorrectlyAllCollection() {
        List<String> list = new ArrayList<>();
        list.add("trutututu");
        list.add("falalala");
        assertTrue(myListStrings.addAll(list));
        Object[] objects = (Object[]) myListStrings.getTab();
        String[] strings = new String[] {"ma", "ta", "bla", "kiki", "ma", "koko", "trutututu", "falalala"};
        for (int i = 0; i < strings.length; i++) {
            assertEquals(strings[i], objects[i]);
        }
        
        List<Person> listOfPerson = new ArrayList<>();
        listOfPerson.add(new Person("Bolek", 24, true));
        listOfPerson.add(new Person("Wojtek", 13, false));
        assertTrue(myListPersons.addAll(listOfPerson));

        List<String> list2 = new ArrayList<>();
        list2.add("trutututu");
        list2.add(null);
        list2.add("falalala");
        
        
        String[] s = new String[] {"mama", "tata"};
        MyList<String> myList = new MyList<>(s);
        assertTrue(myList.addAll(list2));
        Object[] objects2 = (Object[]) myList.getTab();
        String[] strings2 = new String[] {"mama", "tata", "trutututu", null, "falalala"};
        for (int i = 0; i < strings2.length; i++) {
            assertEquals(strings2[i], objects2[i]);
        }
        
    }
    
    @Test
    public void doesAddCorrectlyAllCollectionaAtParticularIndex() {
        List<String> list = new ArrayList<>();
        list.add("trutututu");
        list.add("falalala");
        assertTrue(myListStrings.addAll(1,list));
        Object[] objects = (Object[]) myListStrings.getTab();
        String[] strings = new String[] {"ma", "trutututu", "falalala", "ta", "bla", "kiki", "ma", "koko"};
        for (int i = 0; i < strings.length; i++) {
            assertEquals(strings[i], objects[i]);
        }
        
        List<Person> listOfPerson = new ArrayList<>();
        listOfPerson.add(new Person("Bolek", 24, true));
        listOfPerson.add(new Person("Wojtek", 13, false));
        assertTrue(myListPersons.addAll(1, listOfPerson));

        List<String> list2 = new ArrayList<>();
        list2.add("trutututu");
        list2.add(null);
        list2.add("falalala");
        
        String[] s = new String[] {"mama", "tata"};
        MyList<String> myList = new MyList<>(s);
        assertTrue(myList.addAll(1, list2));
        Object[] objects2 = (Object[]) myList.getTab();
        String[] strings2 = new String[] {"mama", "trutututu", null, "falalala", "tata"};
        for (int i = 0; i < strings2.length; i++) {
            assertEquals(strings2[i], objects2[i]);
        }
    }
    
    @Test
    public void doesGetCorrectly() {
        assertEquals("ma", myListStrings.get(0));
        Person person = new Person("Ala", 24, true);
        assertEquals("Ala", myListPersons.get(0).getName());        
        
        MyList<String> myList = new MyList<>(new String[] {"mama", null, "tata"});
        assertEquals(null, myList.get(1));
    }
    
    @Test
    public void doesSetParticularElement() {
        assertEquals("bla", myListStrings.set(2, "siano"));
        Person person = new Person("Ala", 24, true);
        assertEquals(person, myListPersons.set(0, new Person("Bolek", 34, true)));

        MyList<String> myList = new MyList<>(new String[] {"mama", null, "tata"});
        assertEquals("mama", myList.set(0, null));
        Object[] objects = (Object[]) myList.getTab();
        String[] strings = new String[] {null, null, "tata"};
        for (int i = 0; i < strings.length; i++) {
            assertEquals(strings[i], objects[i]);
        }
    }
    
    @Test
    public void doesReturnCorrctIndex() {
        String[] strings = new String[] {null, "bla", "kiki", "ma", "koko"};
        MyList<String> myListStrings = new MyList<>(strings);
        assertEquals(0, myListStrings.indexOf(null)); 
        assertEquals(0, myListPersons.indexOf(new Person("Ala", 24, true)));
    }
    
    @Test
    public void doesReturnCorrectLastIndex() {
        String[] strings = new String[] {null, "bla", "kiki", "ma", "koko"};
        MyList<String> list = new MyList<>(strings);
        assertEquals(0, list.lastIndexOf(null));
        assertEquals(4, myListStrings.lastIndexOf("ma"));
        assertEquals(1, myListPersons.lastIndexOf(new Person("Ala", 24, true)));
    }
    
    @Test
    public void doesCreateCorrectlyTheListIterator() {
        ListIterator<String> stringListIterator = myListStrings.listIterator();        
        int index = 0;
        while (stringListIterator.hasNext()) {
            assertEquals(stringListIterator.next(), strings[index]);
            index++;
        }
    }
    
    @Test
    public void doesCreateCorrectlyListIteratorFromParticularPoint() {
        ListIterator<String> stringListIterator = myListStrings.listIterator(1);
        int index = 1;
        while (stringListIterator.hasNext()) {
            assertEquals(stringListIterator.next(), strings[index]);
            index++;
        }
    }
    
    @Test
    public void doesCreateCorrectlySubList() {
        int x = 1;
        int y = 3;
        List<String> subList = myListStrings.subList(x, y);
        int index = 0;
        for (int i = x; i < y; i++) {
            assertEquals(subList.get(index), strings[i]);
            index++;
        }
        int counter = 0;
        List<Person> subListNextExample = myListPersons.subList(x, y);
        for (int i = x; i < y; i++) {
            assertEquals(subListNextExample.get(counter), persons[i]);
            counter++;
        }
    }
    
    @Test
    public void doesRetain() {
        List<String> collection = new ArrayList<>();
        collection.add("ma");
        collection.add("kiki");
        assertTrue(myListStrings.retainAll(collection));

        List<Person> listOfPerson = new ArrayList<>();
        listOfPerson.add(new Person("Ala", 24, true));
        listOfPerson.add(new Person("Giena", 67, false));
        assertTrue(myListPersons.retainAll(listOfPerson));
    }
    
    @Test
    public void doesRemoveAll() {
        List<String> collection = new ArrayList<>();
        collection.add("ma");
        collection.add("kiki");
        assertTrue(myListStrings.removeAll(collection));

        String[] strings = new String[] {"ta", "bla", "koko"};
        Object[] objects = (Object[]) myListStrings.getTab();
        
        for (int i = 0; i < strings.length; i++) {
            assertEquals(strings[i], objects[i]);
        }
        
    }
    
    @Test
    public void doesClearCorrectly() {
        String[] tmp = new String[] {};
        myListStrings.clear();
        Object[] objects = (Object[]) myListStrings.getTab();
        assertEquals(tmp.length, objects.length);

        String[] s = new String[] {"mama", "tata", null};
        MyList<String> myList = new MyList<>(s);
        myList.clear();
        Object[] objects2 = (Object[]) myList.getTab();
        assertEquals(tmp.length, objects2.length);
    }

    @Test
    public void doesAddAtParticularIndex() {
        myListStrings.add(1, "NYC");
        Object[] objects = (Object[]) myListStrings.getTab();
        String[] strings = new String[] {"ma",  "NYC", "ta", "bla", "kiki", "ma", "koko"};
        for (int i = 0; i < strings.length; i++) {
            assertEquals(strings[i], objects[i]);
        }        
    }
    
    @Test
    public void doesRemoveAtParticularIndex() {           
        assertEquals("kiki", myListStrings.remove(3));
        String[] strings = new String[] {"ma",  "ta", "bla", "ma", "koko"};
        Object[] objects = (Object[]) myListStrings.getTab();
        for (int i = 0; i < strings.length; i++) {
            assertEquals(strings[i], objects[i]);
        }
    }
    
    @Test
    public void doesInteratorWorksCorrectly() {
        String[] strings = new String[] {"ma", "ta", "bla", "kiki", "ma", "koko"};
        Itr<String> itr = (Itr<String>) myListStrings.iterator();
        int index = 0;
        while (itr.hasNext()) {
            assertEquals(strings[index], itr.next());
            index++;
        }
    }

    
   
}
