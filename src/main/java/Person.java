public class Person {

    private String name;
    private int age;
    private boolean isMarried;

    public boolean isMarried() {
        return isMarried;
    }

    
    
    public Person(String name, int age, boolean isMarried){
        this.name = name;
        this.age = age;
        this.isMarried = isMarried;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    

    public void setMarried(boolean married) {
        isMarried = married;
    }

    @Override 
    public boolean equals(Object o) {        
        if (!(o instanceof Person)) {
            return false;
        }        
        Person p = (Person) o;        
        return p.getName().equals(this.getName()) 
                && p.getAge() == this.getAge() 
                && p.isMarried == this.isMarried;
    }
    
    @Override
    public String toString() {
        return getName() + " " + getAge() + " " + isMarried();
    }
}
