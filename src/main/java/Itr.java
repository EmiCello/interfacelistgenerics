import java.util.Iterator;
import java.util.NoSuchElementException;

class Itr<T> implements Iterator<T> {  
    
    private T[] current;
    int index;
    
    public Itr(T[] tab) {
        this.current = tab;
        this.index = 0;
    }    
    
    @Override
    public boolean hasNext() {
        return index < current.length;
    }

    @Override
    public T next() {
        if (!hasNext()) {
            throw new NoSuchElementException();
        }
        return current[index++];
    }
    
}
