import java.util.*;
import java.util.ListIterator;
import java.util.List;

public class MyList<T> implements List<T>, Iterable<T> {
    
        private T[] tab;

        public MyList(T[] tab) {         
            this.tab = tab;
        }

        public MyList() {
            this.tab = (T[]) new Object[] {};
        }

        @Override
        public Iterator<T> iterator() {            
            return  new Itr<>(this.tab);
        }

        @Override
        public int size() {            
            return tab == null ? 0 : tab.length;
        }

        @Override
        public boolean isEmpty() {
            return this.tab.length == 0;
        }

        @Override
        public boolean contains(Object o) {           
            for (T t : tab) {
                if (Objects.equals(t, o)) {
                    return true;
                }
            }
            return false;
        }
        
        @Override
        public Object[] toArray() {         
            return getTab();
        }

        @Override
        public <T1> T1[] toArray(T1[] t1s) throws ArrayStoreException, NullPointerException {              
            
            if (t1s == null) {
                throw new NullPointerException("t1s is null");
            }
            if (!t1s.getClass().isAssignableFrom(this.tab.getClass())) {
                throw new ArrayStoreException("types are different");
            }

            if (this.tab.length <= t1s.length) {
                for (int i = 0; i < t1s.length; i++) {
                    t1s[i] = i < this.tab.length ? (T1)this.tab[i] : null;
                }
                return t1s;
            } else {
                return Arrays.copyOf((T1[]) this.tab, this.tab.length);
            }    
        } 
        
    
        @Override
        public boolean add(T t) {
            Object[] expandedTab = new Object[this.tab.length + 1];
            for (int i = 0; i < expandedTab.length; i++) {
                expandedTab[i] = (i == expandedTab.length - 1) ? t : this.tab[i]; 
            }  
            setTab((T[]) expandedTab);
            return true;
        }
    
        @Override
        public boolean remove(Object o) {             
            if (this.contains(o)) {
                Object[] tmp = new Object[this.tab.length - 1];                
                int index = 0;
                boolean appeared = false;
                for (int i = 0; i < tmp.length; i++){
                    tmp[i] = (Objects.equals(this.tab[index], o) && !appeared) ? this.tab[index + 1] : this.tab[index]; 
                    if (Objects.equals(this.tab[index], o) && !appeared) {                        
                        appeared = true;
                        index =+ 2;
                    } else {
                        index++;
                    }
                }
                setTab((T[]) tmp);
                return true;
            } 
            return false;
        }
    
        @Override
        public boolean containsAll(Collection<?> collection) {
            Object[] objects = collection.toArray();
            boolean[] contains = new boolean[collection.size()];

            for (int i = 0; i < objects.length; i++) {
                if (!this.contains(objects[i])) {
                    return false;
                }
            }
            return true;
        }
    
        @Override
        public boolean addAll(Collection<? extends T> collection) { 
            int indexOfLastElementInTab = tab.length - 1;
            Object[] tabToAdd = collection.toArray();
            Object[] newTab = new Object[tab.length + tabToAdd.length];            
            for (int i = 0; i < newTab.length; i++) {
                if(i > indexOfLastElementInTab) {
                    newTab[i] = tabToAdd[i - this.tab.length];
                } else {
                    newTab[i] = this.tab[i];
                }
            }
            setTab((T[]) newTab);            
            return true;            
        }
    
        @Override
        public boolean addAll(int x, Collection<? extends T> collection) {
            Object[] tabToAdd = collection.toArray();
            Object[] newTab = new Object[tabToAdd.length + this.tab.length];
            for (int i = 0; i < newTab.length; i++) {
                if (i >= x + tabToAdd.length) {
                    newTab[i] = this.tab[i - tabToAdd.length];
                } else if (i >= x) {
                    newTab[i] = tabToAdd[i - x];
                } else {
                    newTab[i] = this.tab[i];
                }
            }
            setTab((T[]) newTab);            
            return true;
        }
    
        @Override
        public void clear() {
            if (this.tab.length != 0) {
                Object[] tmp = new Object[] {};
                setTab((T[]) tmp);
            }
        }
    
        @Override
        public T get(int x) {
            for (int i = 0; i < this.tab.length; i++) {
                if (i == x) {
                    return this.tab[i];
                }
            }
            return null;
        }
    
        @Override
        public T set(int x, T t) {
            T oldElement = null;
            for (int i = 0; i < this.tab.length; i++) {
                if (i == x) {
                    oldElement = this.tab[i];
                    this.tab[i] = t;
                    
                }
            }            
            return oldElement;
        }
    
        @Override
        public void add(int x, T t) {
            Object[] tmp = new Object[this.tab.length + 1];
            for (int i = 0; i < tmp.length; i++) {
                if (i > x) {
                    tmp[i] = this.tab[i - 1];
                } else if (i == x) {
                    tmp[i] = t;
                } else {
                    tmp[i] = this.tab[i];
                }
            }
            setTab((T[])tmp);         
        }
    
        @Override
        public T remove(int x) {
            Object[] tmp = new Object[this.tab.length - 1];

            int index = 0;
            Object objectToRemowe = null;
            for(int j = 0; j < tmp.length; j++) {
                if (j == x) {
                    objectToRemowe = this.tab[j];
                    index++;
                    tmp[j] = tab[index];
                } else {
                    tmp[j] = tab[index];
                }
                index++;
            }
                setTab((T[]) tmp);
            return (T) objectToRemowe;
        }
    
        @Override
        public int indexOf(Object o) {
            for (int i = 0; i < this.tab.length; i++) {
                if (Objects.equals(this.tab[i], o)) {
                    return i;
                }
            }
            return -1;
        }
    
        @Override
        public int lastIndexOf(Object o) {          
            int lastIndexOfObject = -1;
            for (int i = 0; i < this.tab.length; i++) {                
                if (Objects.equals(this.tab[i], o)) {
                    lastIndexOfObject = i;
                }
            } 
            return lastIndexOfObject != -1 ? lastIndexOfObject : -1;
        }
    
        @Override
        public ListIterator<T> listIterator() {
            ListIterator<T> listItr = null;
            List<T> list = new ArrayList<>();
            for (int i = 0; i < tab.length; i++) {
                list.add(tab[i]);
            }
            listItr = list.listIterator();
            return listItr;
        }
    
        @Override
        public ListIterator<T> listIterator(int x) {
            ListIterator<T> listItr = null;
            List<T> list = new ArrayList<>();
            for (int i = 0; i < tab.length; i++) {
                list.add(tab[i]);
            }
            Iterator firstElement = list.listIterator(x);
            List<T> tmpList = new ArrayList<>();
            while (firstElement.hasNext()) {
                tmpList.add((T)firstElement.next());
            }
            listItr = tmpList.listIterator();            
            return listItr;
        }
    
        @Override
        public List<T> subList(int x, int y) {
            List<T> list = new ArrayList<>();
            for (T t : this.tab) {
                list.add(t);
            }
            List<T> listToReturn = new ArrayList<>();
            for (int i = x; i < y; i++) {
                listToReturn.add(list.get(i));
            }
            return listToReturn;
        }

    @Override
    public boolean removeAll(Collection<?> collection) {
        Object[] elementsToCheck = collection.toArray();
        List<Integer> indexOfElementsToRemoveFromTab = new ArrayList<>();        
        
        for (int i = 0; i < this.tab.length; i++) {
            for (int j = 0; j < elementsToCheck.length; j++) {
                if (this.tab[i].equals(elementsToCheck[j])) {
                    indexOfElementsToRemoveFromTab.add(i);
                }
            }
        }
        
            Object[] newTabWithoutCommonElementsFromCollection = 
                    new Object[this.tab.length - indexOfElementsToRemoveFromTab.size()];
        
            int index = 0;
            while (index < newTabWithoutCommonElementsFromCollection.length) {
                
                for (int i = 0; i < this.tab.length; i++) {
                    if (!indexOfElementsToRemoveFromTab.contains(i)) {
                        newTabWithoutCommonElementsFromCollection[index] = this.tab[i];
                        index++;
                    } 
                }                
            }

            setTab((T[]) newTabWithoutCommonElementsFromCollection);
            if (indexOfElementsToRemoveFromTab.size() != 0) {
                return true;
            } else {
                return false;
            }
    }

        @Override
        public boolean retainAll(Collection<?> collection) {
            Object[] objects = collection.toArray();
            List<Integer> indexOfSimilarElements = new ArrayList<>();
            
            for (int i = 0; i < this.tab.length; i++) {
                boolean appeared = false;
                for (int j = 0; j < objects.length; j++) {
                    if (objects[j].equals(tab[i]) && !appeared) {
                        indexOfSimilarElements.add(i);
                        appeared = true;
                    }
                }
            } 
            
            Object[] newTabOfCommonElements = new Object[indexOfSimilarElements.size()];            
            for (int i = 0; i < indexOfSimilarElements.size(); i++) {
                for (int j = 0; j < this.tab.length; j++) {
                    if (j == indexOfSimilarElements.get(i)) {
                        newTabOfCommonElements[i] = this.tab[j];
                    }
                }
            }
            
            setTab((T[]) newTabOfCommonElements);
            
            if (indexOfSimilarElements.size() != 0) {
                return true;
            } else {
                return false;
            }
        }
        
        protected T[] getTab() {
            return tab;
        }

        private void setTab(T[] tab)
        {
            this.tab = tab;
        }

    }

    

